### A Pluto.jl notebook ###
# v0.19.25

using Markdown
using InteractiveUtils

# ╔═╡ c7164d00-e901-11ed-1a76-29920ab72c1e
using Markdown, InteractiveUtils

# ╔═╡ acbdce01-0b0a-4994-91cb-dd20762d36c6
using PlutoUI; PlutoUI.TableOfContents()

# ╔═╡ 13508b06-bc0b-47f5-acd8-ca61dcd6aebf
using CSV, DataFrames, Random, Statistics, Plots, Distances

# ╔═╡ 094b8397-915f-4245-bc53-777ec0a659b3
using MLJ

# ╔═╡ 0e05c9dc-83bb-4201-883b-ac908e9a877e
using MLJMultivariateStatsInterface

# ╔═╡ b3d07f9e-5ebd-432e-a4dc-8360548b8b00
using MLJDecisionTreeInterface

# ╔═╡ 05e95e79-9b0e-4453-8775-610cf5ee3f36
using MLJXGBoostInterface

# ╔═╡ cb5f2d1f-5984-4f74-b9f9-3688298f7825
using StatsBase, Clustering, StatsPlots

# ╔═╡ 8ea88427-52ea-46b8-9af0-310de34bdf1f
using MLJModels

# ╔═╡ 643ff69f-7ef0-431b-a1d1-d0b4df2e4680
using MLJEnsembles

# ╔═╡ e5ba6334-e6d4-4b32-92ff-07e823a09c1d
using CategoricalArrays

# ╔═╡ 9742d2da-1d62-4092-96e7-aa839a5ae656
using MLJBase

# ╔═╡ b3136f52-1f83-4aa4-b0f4-2d01b7c20bb1
using DecisionTree

# ╔═╡ d2c6288a-778c-4ff7-a347-e5255083e661
using ScikitlearnBase

# ╔═╡ b265210e-dd56-454f-ba91-b681d91d7b98
using ScikitLearn, fit!, predict

# ╔═╡ 34549e57-9d35-40a5-ae9e-c013f0675f84
using ScikiLearnBase

# ╔═╡ 28aa7ab3-6eb3-4ac5-a0c0-bb227ffad90c
#Reading the wine custering data from CSV file
Wc_Data = CSV.read("C:\\Users\\Bertha Vapopya\\Desktop\\Master of Data Science\\Artificial intelligence\\Assignment 2\\wine-clustering.csv",DataFrame)


# ╔═╡ cc9a15f9-cd41-4786-8d7e-8dcb666e52ba
names(Wc_Data)

# ╔═╡ d5b1584c-a0f5-4458-97e5-5ddd7f55a91a
#Decribing the wine clustering dataset
describe(Wc_Data)

# ╔═╡ 80a0d6a1-e2ab-4b85-b17e-1493542fd6f2
describe(Wc_Data, :mean, :std)

# ╔═╡ c761cb5f-65a5-4e4d-9a78-efbacbcffeaa
# Changing both Prloline and mangensium to continuous
Df = coerce(Wc_Data, :Proline=>Continuous, :Magnesium=>Continuous)

# ╔═╡ f39344f5-1c44-494c-a4f1-ddac9b6484bd
#Extracting Features from the dataframe
ftures = Matrix(Df[:, 2:end])

# ╔═╡ e622ea52-32aa-4310-8816-9e2a620035eb
#Calculate the pairwise distances between data points by Eucliedean distance approach
distances = pairwise(Euclidean(), ftures' ,dims=2)

# ╔═╡ bf3d3444-091f-4271-ab33-eefb4cc512dd
#agglomerative hierarchical clustering using's Linkage
h_clustering = hclust(distances, linkage=:ward)

# ╔═╡ 305c8dfc-f30a-4caa-bd2c-d70c6283118f
# Determining the optimal number of clusters by using the elbow method of fuction
ssd = zeros(size(ftures, 1))

# ╔═╡ 6e74dd8a-6a49-4c73-9dc4-e0af13f26321
function elbow(y::Vector)
    x = 1:length(y)
    slopes = [abs((y[i]-y[1])/(x[i]-x[1])) for i in 2:length(y)]
    max_slope_idx = argmax(slopes)
    return x[max_slope_idx+1]
end

# ╔═╡ 8c9f079b-57fe-41fd-93cd-6db52a0aa882
k = 3

# ╔═╡ ad4970a1-b5a7-42e1-b4f7-7f2b7cdd650d
clustering = cutree(h_clustering, k=k)

# ╔═╡ 13d6ac3e-3f5c-47df-b84c-97d08c941f74
#Print the cluster labels for each data point
println("Cluster labels:", clustering')

# ╔═╡ a4429586-c99d-4d08-b783-303540a3ddf0


# ╔═╡ 8c0f80d0-8cda-48d0-84eb-eedeaed365dc
plot(h_clustering ,xlabel="Data Points",ylabel="Distance",title="Hierarchical Clustering Dendrogram")

# ╔═╡ df2be6c3-5be9-446b-a562-e72961db0885
scatter(Df[:, 1],Df[:, 2], color=clustering, legend=false, xlabel="Wine Type",ylabel="distance", title="Wine Clusters")

# ╔═╡ d7db1d51-4dc1-423b-97d3-730ff8109104
#Split the dataset into training and test
train, test =partition(eachindex(clustering), 0.7, shuffle=true, rng=123)

# ╔═╡ 1a9ee606-caa5-429b-a8f9-5f8d1f98cb92
X_train = Df[train, :]

# ╔═╡ 6fd9a63c-1534-41c2-ad0e-788d6178d791
Y_train = clustering[train]

# ╔═╡ 9feec720-b0ef-4d1b-84c5-c9c0ba165733
X_test = Df[test, :]

# ╔═╡ 802c0256-5566-4f13-98ef-6e5246a2f521
Y_test = clustering[test]

# ╔═╡ cacad3e8-0f9c-4efc-9958-18086d310088


# ╔═╡ 94288b48-b8a9-4c5e-94aa-b9de0eedd9b7
@sk_import ensemble: AdaBoostClassifier

# ╔═╡ b7d4ce13-0a1c-437f-b7dd-7e1ece996731
#AdaBoost classifier
ada_model = AdaBoostClassifier(n_estimators=100)

# ╔═╡ d1c30b39-9a8f-4f14-bebe-cb4474cc2b87
ScikitlearnBase.fit!(ada_model, X_train, Y_train)

# ╔═╡ f30ad896-2fda-428d-82ff-e4eee8bfe167
y_pred = ScikitlearnBase.predct(ada_model,X_test)

# ╔═╡ 7997be01-29bb-4870-920b-660b75661a7a
#Testing / calculating the accurancy score of the model
acc = MLJBase.accuracy(y_pred, y_test)

# ╔═╡ Cell order:
# ╠═993eff5a-ec14-4e90-94dd-adbaba5739fe
# ╠═c7164d00-e901-11ed-1a76-29920ab72c1e
# ╠═acbdce01-0b0a-4994-91cb-dd20762d36c6
# ╠═13508b06-bc0b-47f5-acd8-ca61dcd6aebf
# ╠═094b8397-915f-4245-bc53-777ec0a659b3
# ╠═0e05c9dc-83bb-4201-883b-ac908e9a877e
# ╠═b3d07f9e-5ebd-432e-a4dc-8360548b8b00
# ╠═05e95e79-9b0e-4453-8775-610cf5ee3f36
# ╠═cb5f2d1f-5984-4f74-b9f9-3688298f7825
# ╠═8ea88427-52ea-46b8-9af0-310de34bdf1f
# ╠═643ff69f-7ef0-431b-a1d1-d0b4df2e4680
# ╠═e5ba6334-e6d4-4b32-92ff-07e823a09c1d
# ╠═9742d2da-1d62-4092-96e7-aa839a5ae656
# ╠═b3136f52-1f83-4aa4-b0f4-2d01b7c20bb1
# ╠═d2c6288a-778c-4ff7-a347-e5255083e661
# ╠═b265210e-dd56-454f-ba91-b681d91d7b98
# ╠═28aa7ab3-6eb3-4ac5-a0c0-bb227ffad90c
# ╠═cc9a15f9-cd41-4786-8d7e-8dcb666e52ba
# ╠═d5b1584c-a0f5-4458-97e5-5ddd7f55a91a
# ╠═80a0d6a1-e2ab-4b85-b17e-1493542fd6f2
# ╠═c761cb5f-65a5-4e4d-9a78-efbacbcffeaa
# ╠═f39344f5-1c44-494c-a4f1-ddac9b6484bd
# ╠═e622ea52-32aa-4310-8816-9e2a620035eb
# ╠═bf3d3444-091f-4271-ab33-eefb4cc512dd
# ╠═305c8dfc-f30a-4caa-bd2c-d70c6283118f
# ╠═6e74dd8a-6a49-4c73-9dc4-e0af13f26321
# ╠═ad4970a1-b5a7-42e1-b4f7-7f2b7cdd650d
# ╠═8c9f079b-57fe-41fd-93cd-6db52a0aa882
# ╠═13d6ac3e-3f5c-47df-b84c-97d08c941f74
# ╠═a4429586-c99d-4d08-b783-303540a3ddf0
# ╠═8c0f80d0-8cda-48d0-84eb-eedeaed365dc
# ╠═df2be6c3-5be9-446b-a562-e72961db0885
# ╠═d7db1d51-4dc1-423b-97d3-730ff8109104
# ╠═1a9ee606-caa5-429b-a8f9-5f8d1f98cb92
# ╠═6fd9a63c-1534-41c2-ad0e-788d6178d791
# ╠═9feec720-b0ef-4d1b-84c5-c9c0ba165733
# ╠═802c0256-5566-4f13-98ef-6e5246a2f521
# ╠═cacad3e8-0f9c-4efc-9958-18086d310088
# ╠═94288b48-b8a9-4c5e-94aa-b9de0eedd9b7
# ╠═34549e57-9d35-40a5-ae9e-c013f0675f84
# ╠═b7d4ce13-0a1c-437f-b7dd-7e1ece996731
# ╠═d1c30b39-9a8f-4f14-bebe-cb4474cc2b87
# ╠═f30ad896-2fda-428d-82ff-e4eee8bfe167
# ╠═7997be01-29bb-4870-920b-660b75661a7a
